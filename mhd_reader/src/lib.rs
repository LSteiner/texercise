use std::fs;
use std::path::Path;
use image;
use file_diff::diff;

pub fn get_img_dims_from_mhd(mhd_path: &String) -> [u32;3]{
    let contents = fs::read_to_string(mhd_path).expect("Error reading file");

    let split = contents.split("\n");

    let mut dims: Vec<&str> = ["",""].to_vec();

    for s in split {
        if s.contains("DimSize = "){
            dims = s.split(" = ").collect();
        }
    }

    dims = dims[1].split(" ").collect();
    let mut result: [u32;3] = [0,0,0];

    for i in 0..3{
        result[i] = dims[i].trim().parse::<u32>().expect("Error converting strings");
    }

    return result;
}

pub fn get_file_as_byte_vec(filename: &String) -> Vec<u8> {
	let buffer = std::fs::read(filename).unwrap();
	return buffer
}

pub fn convert_16bit_lend_data_to_8bit_vec(data_vec: &Vec<u8>) -> Vec<u8>{
	let mut result_8: Vec<u8> = vec![0; data_vec.len()/2];

	let mut curr_result_8: u8;

	for i in 0..data_vec.len()/2{
		let index = i as usize;

		curr_result_8 =  ((data_vec[index*2+1] ) << 4) | (data_vec[index*2] >> 4);

		result_8[index] = curr_result_8;

	}
	return result_8;
}

pub fn write_buffer_to_gray_img(path: &Path, buffer: &[u8], size_1: &u32, size_2: &u32){
	image::save_buffer(path, buffer, *size_1, *size_2, image::ColorType::L8).expect("Something went wrong"); 
}

pub fn save_3d_img_slice_to_png(img_path: &Path, img_data: &Vec<u8>, size_x: &u32, size_y: &u32, size_z: &u32, direction: u8){
    let mut dim1: u32 = 0;
    let mut dim2: u32 = 0;

	if direction == 0 {
        dim1 = *size_y;
        dim2 = *size_z;
    }

	else if direction == 1{
        dim1 = *size_x;
        dim2 = *size_z;
    }

	else if direction == 2{
        dim1 = *size_x;
        dim2 = *size_y;
    }

	let mut slice_data: Vec<u8> = vec![0; (dim1*dim2) as usize];
	
    if direction == 0{
        let i = size_x/2;
        let mut km;

        for k in 0..*size_z{
            for j in 0..*size_y {
                    km =  size_z - k - 1;
                    slice_data[(j + km * size_y) as usize] = img_data[(i + j * size_x + k * size_x * size_y) as usize];
            }

        }

	}

	else if direction == 1{
        let j = size_y/2;
        let mut km;

        for k in 0..*size_z{
            for i in 0..*size_x {
                    km =  size_z - k - 1;
                    slice_data[(i + km * size_x) as usize] = img_data[(i + j * size_x + k * size_x * size_y) as usize];
            }

        }
	}

	else if direction == 2{
		let half_height = size_z/2;

		slice_data = img_data[(half_height*size_x*size_y) as usize..((half_height+1)*(size_x*size_y)) as usize].iter().cloned().collect();
	}

    write_buffer_to_gray_img(img_path, &slice_data, &dim1, &dim2);
}

#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn test_get_img_dims_from_mhd(){
        let test_path = String::from("./data/sinus.mhd");
        assert_eq!(get_img_dims_from_mhd(&test_path), [512,512,333]);
    }

	#[test]
	fn test_get_file_as_byte_vec(){
        let test_path = String::from("./data/sinus.raw");
        assert_eq!(get_file_as_byte_vec(&test_path)[0..10], [1,0,1,0,1,0,1,0,1,0]);
    }

	#[test]
	fn test_convert_16bit_lend_data_to_8bit_vec(){
        assert_eq!(convert_16bit_lend_data_to_8bit_vec(&vec![0,0, 1,0, 15,0, 16,0, 255,1, 255,15, 255,17, 255,255]), 
															[0,   0,   0,    1,    31,    255,    31,     255    ]);
	}

    #[test]
	fn test_write_buffer_to_gray_img(){
        let test_path = "./src/test_gen.png";
        write_buffer_to_gray_img(&Path::new(test_path), &[0,3,7,15,31,63,127,255], &4, &2);

        assert!(diff("./src/test_gen.png", "./src/test.png"));        
	}

}