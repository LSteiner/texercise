mod lib;
use std::path::Path;

fn main() {
	let mhd_file = String::from("./data/sinus.mhd");

	let raw_file = mhd_file.replace(".mhd", ".raw");

	let [size_x, size_y, size_z] = lib::get_img_dims_from_mhd(&mhd_file);

    let data_vec = lib::get_file_as_byte_vec(&raw_file);

    let result = lib::convert_16bit_lend_data_to_8bit_vec(&data_vec);

	
	lib::save_3d_img_slice_to_png(&Path::new(&str::replace(&mhd_file, ".mhd", "-z.png")), &result, &size_x, &size_y, &size_z, 2);

	lib::save_3d_img_slice_to_png(&Path::new(&str::replace(&mhd_file, ".mhd", "-y.png")), &result, &size_x, &size_y, &size_z, 1);

	lib::save_3d_img_slice_to_png(&Path::new(&str::replace(&mhd_file, ".mhd", "-x.png")), &result, &size_x, &size_y, &size_z, 0);    
}
